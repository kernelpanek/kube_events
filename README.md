## kube_events

I wrote this simple go application because I disliked the options available to watch Kubernetes events with `kubectl`.  I wanted the output to be colorized in a way that would make warning events stand out from normal events.

This app is 100% opionated for now but could be improved in the future:
* Allow for user options on:
    * Colors
    * Cluster context
* Allow logging to file and standard out sans color escape commands in file data
* Add jsonpath or jq-styled query filtering


### Compile

```bash
> cd src/tools/k8s-tail
> go get -u github.com/Masterminds/glide
> glide install
> go build
> ./k8s-tail
```

### Features
* Colorized output
* Event Filtering (see only the events you care about)


#### Event Filtering
The current set of operators are limited to the operators available in Kubernetes client-go library. The Kubernetes API machinery supports more operators than the client-go library and limit this application's ability to use the additional operators. Currently, the application only supports `in` and `not in` (equals, not equals if the full length of value is used).

```bash
> kube_events -filter InvolvedObject.Name=fluentd
```

```bash
> kube_events -filter Metadata.Name=fluentd,Metadata.Namespace=kube-system,Reason=BackOff
```

#### List of Filterable fields

```yaml
- Metadata
  - Name
  - Namespace
  - SelfLink
  - Uid
  - ResourceVersion
  - CreationTimestamp
- InvolvedObject
  - Kind
  - Name
  - Namespace
  - Uid
  - ApiVersion
  - ResourceVersion
  - FieldPath
- Reason
- Message
- Source
  - Component
  - Host
- FirstTimestamp
- LastTimestamp
- Count
- Type
```
