package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"
	"flag"
	"path/filepath"
	"reflect"
	_ "k8s.io/client-go/plugin/pkg/client/auth/oidc"
	c "github.com/TreyBastian/colourize"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"github.com/ryanuber/columnize"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/cache"
	"k8s.io/apimachinery/pkg/fields"
	"k8s.io/api/core/v1"
	"bytes"
	"k8s.io/apimachinery/pkg/selection"
)
var (
	kubeconfigFlag *string
	contextFlag string
	selectorsFlag string
	filtersFlag string
	debugFlag bool
	orFlag bool
)

func init() {
	if home := os.Getenv("HOME"); home != "" {
		kubeconfigFlag = flag.String("kubeconfig", filepath.Join(home, ".kube", "config"), "(optional) absolute path to the kubeconfig file")
	} else {
		kubeconfigFlag = flag.String("kubeconfig", "", "absolute path to the kubeconfig file")
	}
	flag.StringVar(&contextFlag, "context", "", "context")
	flag.StringVar(&selectorsFlag, "l", "", "selectors")
	flag.StringVar(&filtersFlag, "filter", "", "filters")
	flag.BoolVar(&debugFlag, "debug", false, "enable debugging information")
	flag.BoolVar(&orFlag, "or", false, "allows any filter")
	flag.Parse()
}

func getNestedField(objectValue reflect.Value, path []string) interface{} {
	if len(path) > 0 {
		field := objectValue.FieldByName(path[0])

		indirectField := reflect.Indirect(field)
		switch indirectField.Kind() {
		case reflect.Struct:
			return getNestedField(field, path[1:])
		default:
			return field.Interface()
		}

	} else {
		return nil
	}
}

func any(truths []bool) bool {
	var result bool = false
	for _, t := range truths {
		if t {
			result = true
		}
	}
	return result
}

func all(truths []bool) bool {
	var result bool = true
	for _, t := range truths {
		if !t {
			return false
		}
	}
	return result
}

func evaluate(evt *v1.Event, req fields.Requirement) bool {
	//if debugFlag {
	//	combo := "AND"
	//	if or {
	//		combo = "OR"
	//	}
	//	fmt.Printf("%s %s %v %s\n", combo, req.Field, req.Operator, req.Value)
	//}
	var result bool
	fieldPath := strings.Split(req.Field, ".")
	event := reflect.ValueOf(evt).Elem()
	switch req.Operator {
	case selection.Equals:
		if strings.Contains(getNestedField(event, fieldPath).(string), req.Value) {
			result = true
		} else {
			result = false
		}
	case selection.DoubleEquals:
		if strings.Contains(getNestedField(event, fieldPath).(string), req.Value) {
			result = true
		} else {
			result = false
		}
	case selection.NotEquals:
		if !strings.Contains(getNestedField(event, fieldPath).(string), req.Value) {
			result = true
		} else {
			result = false
		}
	}
	return result
}

func processEvent(evt *v1.Event, fltr fields.Selector) {

	if fltr == nil || len(fltr.Requirements()) == 0 {
		displayEvent(evt)
	} else {
		var evaluations []bool
		var requirements = fltr.Requirements()
		for _, req := range requirements {
			evaluations = append(evaluations, evaluate(evt, req))
		}

		if orFlag {
			if any(evaluations) {
				displayEvent(evt)
			}
		} else {
			if all(evaluations) {
				displayEvent(evt)
			}
		}


	}
}

func displayEvent(evt *v1.Event) {
	var messageColor int
	switch evt.Type {
	case "Normal":
		messageColor = c.Grey
	case "Warning":
		messageColor = c.Red
	}

	printOut := make([]string, 1)
	printOut[0] = strings.Join([]string{
		c.Colourize(evt.LastTimestamp.String(), c.White),
		fmt.Sprintf("%s / %s",
			c.Colourize(evt.Type, messageColor),
			c.Colourize(evt.Reason, c.Bold, c.Cyan),
		),
		c.Colourize(evt.Source.Component, c.Green),
		c.Colourize(evt.Source.Host, c.Bold, c.Yellow),
		c.Colourize(
			fmt.Sprintf("%s::%s::%s",
				evt.InvolvedObject.Namespace,
				evt.InvolvedObject.Kind,
				evt.InvolvedObject.Name,
			), c.Bold, c.Cyan),
		strconv.Itoa(int(evt.Count))}, "|")
	result := columnize.SimpleFormat(printOut)

	fmt.Println(c.Colourize(result, messageColor))
	fmt.Println(c.Colourize(evt.Message, messageColor))
	fmt.Println("-----")

	if debugFlag {
		jEvtBytes, err := json.Marshal(evt)
		if err == nil {
			var out bytes.Buffer
			json.Indent(&out, jEvtBytes, "", "  ")
			fmt.Println(c.Colourize(fmt.Sprintf("%+v", string(out.Bytes())), c.Yellow))
		} else {
			fmt.Println(err)
		}
		fmt.Println("-----")
	}
}

func main() {

	var configOverrides = clientcmd.ConfigOverrides{ClusterInfo: clientcmdapi.Cluster{Server: ""}, Context: clientcmdapi.Context{Cluster: contextFlag }}

	config, err := clientcmd.NewNonInteractiveDeferredLoadingClientConfig(
		&clientcmd.ClientConfigLoadingRules{ExplicitPath: *kubeconfigFlag},
		&configOverrides).ClientConfig()

	if err != nil {
		panic(err.Error())
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		fmt.Println("Error with connecting to cluster:", err.Error())
	}

	var selectors fields.Selector
	if sel, err := fields.ParseSelector(selectorsFlag); err == nil {
		selectors = sel
	} else {
		selectors = fields.Everything()
	}

	var filters fields.Selector
	if fltr, err := fields.ParseSelector(filtersFlag); err == nil {
		filters = fltr
	} else {
		fmt.Println(err)
		filters = nil
	}

	watchList := cache.NewListWatchFromClient(clientset.CoreV1().RESTClient(), "events", metav1.NamespaceAll, selectors)
	_, controller := cache.NewInformer(watchList, &v1.Event{}, time.Second * 0,
		cache.ResourceEventHandlerFuncs{
			AddFunc: func(obj interface{}) {
				evt, ok := obj.(*v1.Event)
				if ok {
					processEvent(evt, filters)
				}
			},
			DeleteFunc: func(obj interface{}) {
				evt, ok := obj.(*v1.Event)
				if ok {
					processEvent(evt, filters)
				}
			},
			UpdateFunc: func(oldObj, newObj interface{}) {
				evt, ok := newObj.(*v1.Event)
				if ok {
					processEvent(evt, filters)
				}
			},
		})
	stop := make(chan struct{})

	go controller.Run(stop)

	for {
		time.Sleep(time.Second)
	}
}
