default:
	go get -u github.com/Masterminds/glide
	glide update
	go build

test:
	#empty test

lint:
	go get -u github.com/golang/lint/golint
	golint
